# clangFormatterExample

To są moje przykładowe "idealnie sformatowane" kody na podstawie których generowany jest plik .clang-format do automatycznego formatowania.

Aktualna wersja "Clang format editor" to: 2024.3.0.

Wykorzystywany program to: "Clang format editor" - https://www.clangpowertools.com/clang-format-editor.html.

Opcje do modyfikacji recznej:

- IndentExternBlock - NoIndent
- SpaceAfterCStyleCast - False

Plik .clang-format zawiera aktualny plik.