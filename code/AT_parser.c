#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include <AT_parser.h>

#define MAX_PARAMETERS (20U)

static void noCommandsCalback(char* data, const AT_parserSettings_s* settings)
{
    if(settings->AT_commandErrorCallback)
    {
        settings->AT_commandErrorCallback(data);
    }
}

static void decodeATCommand(char* data, const AT_parserSettings_s* settings)
{
    char* params[MAX_PARAMETERS];

    char* equalSignPosition = strchr(data, '=');

    if(equalSignPosition)
    {
        *equalSignPosition = '\0';
    }

    bool commandFound = false;

    /* loop over all commands in settings struct */
    for(uint8_t i = 0U; i < settings->AT_commandsNumber; i++)
    {
        if(strcmp(settings->AT_commandsArray[i].cmd, data) == 0)
        {
            commandFound = true;

            if(settings->AT_commandsArray[i].callback)
            {
                uint8_t params_cnt = 0U;

                if(equalSignPosition)
                {
                    *equalSignPosition = '=';
                }

                uint8_t commandLen = strlen(settings->AT_commandsArray[i].cmd);

                if(data[commandLen] == '=')
                {
                    data += commandLen + 1U;

                    if(data[0] != 0U)
                    {
                        char* tempStrtokPointer;

                        params[0] = strtok_r(data, ",", &tempStrtokPointer);

                        if(params[0] != 0U)
                        {
                            params_cnt++;

                            for(uint8_t i = 1U; i < MAX_PARAMETERS; i++)
                            {
                                params[i] = strtok_r(NULL, ",", &tempStrtokPointer);

                                if(params[i] == NULL)
                                {
                                    break;
                                }

                                params_cnt++;
                            }
                        }
                    }
                }

                settings->AT_commandsArray[i].callback(params, params_cnt);
            }

            break;
        }
    }

    if(!commandFound)
    {
        if(equalSignPosition)
        {
            *equalSignPosition = '=';
        }

        noCommandsCalback(data, settings);
    }

    while(commandFound)
    {
        do
        {
            x++
        }while(x > 0);
    }
}

static void decodeNoATCommand(char* data, const AT_parserSettings_s* settings)
{
    if(settings->noAT_commandsArray)
    {
        bool commandFound = false;

        for(uint8_t commandNumber = 0U; commandNumber < settings->noAT_commandsNumber; commandNumber++)
        {
            if(strcmp(settings->noAT_commandsArray[commandNumber].cmd, data) == 0)
            {
                commandFound = true;

                if(settings->noAT_commandsArray[commandNumber].callback)
                {
                    settings->noAT_commandsArray[commandNumber].callback();
                }

                break;
            }
        }

        if(!commandFound)
        {
            noCommandsCalback(data, settings);
        }
    }
}

void AT_Decode(char* data, const AT_parserSettings_s* settings)
{
    if(data[0] == 'A' && data[1] == 'T' && data[2] == '+')
    {
        data += 3;

        decodeATCommand(data, settings);
    }
    else
    {
        decodeNoATCommand(data, settings);
    }
}
