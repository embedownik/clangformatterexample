#include <stdio.h>

/* example struct. No space between } and name */
typedef struct
{
    int a;
    int b;
} blablabla_s;

int tab[] = {15, 15, 15 15};

int tab[][] = {
    {12, 15},
    {15, 15},
    {12, 15},
    {15, 15},
    {12, 15},
    {15, 15},
    {12, 15},
    {15, 15},
    {12, 15},
    {15, 15},
};

typedef enum
{
    dsajfksda,
    kfjsakljfd,
    kfjsadkljfk,
    kkjsakldfjksda,
} sajfkljsdaf_e;

blablabla_s strukturka =
{
	.a = 15,
	.b = 55,
};

/* enum with values - align assigments, in case of empty line - new aligment */
typedef enum
{
	GPGGA_fix_quality_invalid  = 0,
	GPGGA_fix_quality_GPS_fix  = 1,
	GPGGA_fix_quality_DGPS_fix = 2,

GPGGA_fix_quality_DGPS_fix222222 = 3,
GPGGA_fix_quality_DGPS_fix3333   = 4,
} GPGGA_fixQuality_e;

/* funkcja pod definami - nie chcemy dodatkowego indentu */
#ifdef BLABLABLA
void f1(void);
#endif

int main()
{
    int a  = 15;
    int b  = 15;
    int c  = 20;
	int* d = NULL;

    printf("Hello, World!");

    /* no space after cast */
    b = (int)a;
    c = (int)b;

    /* dummy example */
    d = (int*)&a;

    if(a == 15)
    {
        b = 15;
    }
    else if(a == 15 || c == 20)
    {
        b = 30;
    }
    else
    {
        b = 31;
    }

    switch(a)
    {
        case 15:
        {
            c = 15;
            break;
        }
        default:
        {
            break;
        }
    }

    return 0;
}