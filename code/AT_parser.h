#ifndef AT_PARSER_H_INCLUDED
#define AT_PARSER_H_INCLUDED

#include <inttypes.h>
#include <libs_config/AT_parser_settings.h>

#ifdef __cplusplus
extern "C"
{
#endif

//! structure for single ATcommand with parameters
typedef struct
{
    const char* cmd;
    void (*callback)(char**, uint8_t);
}atCMD_s;

//! structure for single ATcommand without parameters
typedef struct
{
    const char* cmd;
    void (*callback)(void);
}atCMD_noParam_s;

//! type for callback in case of unknown command with received data as parameter
typedef void (*AT_ErrorCallback_t)(const char*);

//! helpers to fill commands structs easier
#define AT_COMMANDS_AT_TABLE(x)      .AT_commandsArray = x,    .AT_commandsNumber = sizeof(x)/sizeof(x[0])
#define AT_COMMANDS_AT_TABLE_NULL    .AT_commandsArray = NULL, .AT_commandsNumber = 0U

#define AT_COMMANDS_NO_AT_TABLE(x)   .noAT_commandsArray = x,    .noAT_commandsNumber = (sizeof(x)/sizeof(x[0]))
#define AT_COMMANDS_NO_AT_TABLE_NULL .noAT_commandsArray = NULL, .noAT_commandsNumber = 0U

//! parserSettings in one struct
typedef struct
{
    const atCMD_s*         AT_commandsArray;
    const uint8_t          AT_commandsNumber;

    const atCMD_noParam_s* noAT_commandsArray;
    const uint8_t          noAT_commandsNumber;

    const AT_ErrorCallback_t AT_commandErrorCallback;
}AT_parserSettings_s;

/**
 * Decode given text as ATCommand with selected settings/callbacks.
 * @param data     received data to parse, module will modify content of this buffer!
 * @param settings ATcommand settings
 */
void AT_Decode(char* data, const AT_parserSettings_s* settings);

#ifdef __cplusplus
}
#endif

#endif
